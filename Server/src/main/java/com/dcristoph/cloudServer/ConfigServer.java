package com.dcristoph.cloudServer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@EnableConfigServer
@SpringBootApplication
@RestController
public class ConfigServer {

    @Value("${server.port}")
    private String showLocation;

    @GetMapping("/location/")
    public String getShowLocation(){
        return this.showLocation;
    }

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(ConfigServer.class, args);
    }

}
